$(function(){

    // СКРИПТЫ СО СТРАНИЦЫ

    $('.nav-justified li').hover(
        function(){ $(this).addClass('hover') },            // ЛОЛШТО???!!!!
        function(){ $(this).removeClass('hover') }
    );

    function toggleCodes(on) {
        var obj = document.getElementById('icons');
        if (on) {
            obj.className += ' codesOn';
        } else {
            obj.className = obj.className.replace(' codesOn', '');
        }
    }

    $(document).ready(function() {
        if($('.carousel').length){
            $(".carousel").swiperight(function() {
                $(this).carousel('prev');
            });
            $(".carousel").swipeleft(function() {
                $(this).carousel('next');
            });
            $(".carousel").carousel({
                interval:false
            });
        }
    });

    if($('.cutstring').length){
        $('.cutstring').cutstring();
    }

    $("#projectPrev i").click(function(){
        $("#owl-demo").trigger('owl.prev');
        return false;
    })
    $("#projectNext i").click(function(){
        $("#owl-demo").trigger('owl.next');
        return false;
    });

    if($('#owl-demo').length) {
        $("#owl-demo").owlCarousel({
            itemsCustom: [
                [0, 1],
                [600, 2],
                [700, 2],
                [1000, 3],
                [1200, 4]
            ],
            navigation: true,
            navigationText: false
        });
    }

    // КОНЕЦ СКРИПТЫ СО СТРАНИЦЫ

    // подключаем стайлер для селекта на странице
    if($('select').length){
        $('select').styler();
    }

	// fancybox
    if($('.fancy').length){
        $(".fancy").fancybox({});
    }

    if($(".fancy-load").length) {
        $(".fancy-load").fancybox({
            afterLoad: function () {
                var price = $('.project').attr('data-price');
                var text = $('.project').attr('data-text');
                var area = $('.project').attr('data-area');
                var name = $('.project').attr('data-name');
                var img = $('.project').attr('data-img');
                var pos = $('.jq-selectbox__dropdown ul li.selected').index();
                setTimeout(function () {
                    $('.product-form--wrap .product-form__info--img').attr('src', img);
                    $('.product-form--wrap .product-form__info--area').text(area);
                    $('.product-form--wrap .product-form__info--content--price').text(price);
                    $('.product-form--wrap .product-form__info--title strong').text(name);
                    $('.product-form--wrap .form-id').prop('value',name);
                    $('.product-form--wrap .product-form__info--content--text').text(text);
                    $('.product-form--wrap .jq-selectbox__dropdown ul li').eq(pos).trigger('click');
                }, 10)
            }
        });
    }

	// переключение табов и изменение селекта
    $(document).on('click', 'ul.project__tabs--tabs-list li:not(.current)', function() {
		var pos = $(this).index() - 4;
		$(this).addClass('current').siblings().removeClass('current').parents('div.project__tabs').find('div.project__tabs-box').eq($(this).index()).fadeIn(150).siblings('div.project__tabs-box').hide();
		$('.jq-selectbox__dropdown ul li').eq(pos).trigger('click');
	});

    // при клике на селект переключаем таб
	$(document).on('click', '.project .jq-selectbox__dropdown ul li', function(){
		var pos = $(this).index();
		$('.project__tabs--tabs-list li').eq(pos).trigger('click');
	});

	// прокрутка до подробностей
	$(document).on('click', '.js-scroll', function(){
		$("html, body").animate({
			scrollTop: $($(this).attr("href")).offset().top + "px"
		},500);
		return false;
	});

	// ввод только цифр в поле суммы
	$(document).on('keydown', '.input--phone', function(e){input_number();})

	// удаление/добавление placeholder
    $(document).on('focus', '.form--field', function(){
        if ( $(this).attr('placeholder') ) $(this).data('placeholder', $(this).attr('placeholder')).removeAttr('placeholder');
    });
    $(document).on('blur', '.form--field', function(){
		if ( $(this).data('placeholder') ) $(this).attr('placeholder', $(this).data('placeholder')).removeData('placeholder');
	});

	// отправка почты
	$(document).on('click', '.form--submit', function () {
		var form =  $(this).parents('.main-form');
		var errors =  false;
		$(form).find('.required').each(function(){
			var val = $(this).prop('value');
			if(val == ''){
				$(this).parents('label').addClass('error');
				errors = true;
			}
			else{
				if($(this).hasClass('input--mail')){
					if(validateEmail(val) == false){
						$(this).parents('label').addClass('error');
						errors = true;
					}
				}
			}
		});
		if(errors == false){
			var button_value = $(form).find('.form--submit').text();
            var method = form.prop('method');
            var action = form.prop('action');
			$(form).find('.form--submit').text('Подождите...');
			$.ajax({
				type: method,
				url: action,
				data: form.serialize(),
				success: function(data) {
					$(form).fadeOut(200);
					$(form).next('.result').removeClass('hidden-block').html('Ваша заявка отправлена.<br/> В ближайшее время с Вами свяжутся')
				},
				error: function(data) {
					$(form).find('.form--submit').text('Произошла ошибка');
					setTimeout(function() {
						$(form).find('.form--submit').text(button_value);
					}, 2000);
				}
			});

		}
		return false;
	});

	// фокус поля убираем сообщение об ошибке
	$(document).on('focus', '.form--field', function(){
		$(this).parents('label').removeClass('error');
	});

        // ФУНКЦИИ

        // валидация почты
        function validateEmail(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }

        // ввод только цифр в поле
        function input_number (){
            var allow_meta_keys=[86, 67, 65];
            if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9  || event.keyCode == 27 ||
                // Разрешаем Ctrl+A
                ($.inArray(event.keyCode,allow_meta_keys) > -1 && (event.ctrlKey === true ||  event.metaKey === true)) ||
                // Разрешаем home, end, влево, вправо
                (event.keyCode >= 35 && event.keyCode <= 39)) {
                return;
            }
            else {
                // Убеждаемся, что это цифра, и останавливаем событие keypress
                if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                    event.preventDefault();
                }
            }
        };

});